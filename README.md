What Is? is a tool for playing along with Jeopardy! at home. It provides:

- A command-line program for entering a right/wrong/no-answer result for each clue as you watch the show.
- A database for storing your results.
- (Coming Soon) A web application for inspecting and visualizing your stats.

# Setup

Build the docker images with `docker compose build`.

# Playing Along With the Show

When you start watching Jeopardy!, run the command-line program with `docker compose run --rm cli`. You'll be presented with a prompt. Enter `help` for a list of available commands. A typical session is presented below, with user-entered commands indicated in <strong>bold</strong>.

Start a new game that aired on a certain day.

<pre>
--> <strong>new 2018-06-12</strong>
OK: starting new game that aired Tue, Jun 12, 2018
== [ 1] R1 $0 =>
</pre>

Enter the results of some clues. The number represents the clue's dollar amount (in hundreds) and the letter represents the result: "r" means you got it right, "w" means you got it wrong, and "n" means you gave no answer.

<pre>
== [ 1] R1 $0 => <strong>2r</strong>
OK: round 1, right $200
== [ 2] R1 $200 => <strong>4r</strong>
OK: round 1, right $400
== [ 3] R1 $600 => <strong>6n</strong>
OK: round 1, no answer $600
== [ 4] R1 $600 => <strong>8w</strong>
OK: round 1, wrong $800
== [ 5] R1 $-200 => <strong>10r</strong>
OK: round 1, right $1000
== [ 6] R1 $800 =>
</pre>

The Daily Double has been uncovered. Enter the clue's dollar amount on the board, followed by <strong>dd</strong>, your wager, and your result:

<pre>
== [ 6] R1 $800 => <strong>6dd500r</strong>
OK: round 1, right $600 (daily double, wager = $500)
== [ 7] R1 $1300 =>
</pre>

More clues go by, and it's time for the Double Jeopardy! round. Advance the round with the <strong>round</strong> command:

<pre>
== [29] R1 $4200 => <strong>round</strong>
OK: starting round 2
== [29] R2 $4200 =>
</pre>

More clues go by, and it's time for the Final Jeopardy! round. Advance the round with the <strong>round</strong> command:

<pre>
== [59] R2 $7900 => <strong>round</strong>
OK: starting round f
== [59] Rf $7900 =>
</pre>

Record your wager followed by your result:

<pre>
== [59] Rf $7900 => <strong>2100r</strong>
OK: round f, right $2100
== [60] Rf $10000 =>
</pre>

The game is over, so save it and look at your stats:

<pre>
== [60] Rf $10000 => <strong>save</strong>
OK: saving current game
OK: assigning ID 120 to current game
OK: game 120 saved
--> <strong>stat 120</strong>
OK: printing statistics for game 120

Overall:
  Right: 29/59 ( 49.2%)
  Wrong:  8/59 ( 13.6%)
  NoAns: 22/59 ( 37.3%)

...

-->
</pre>

# Running Tests

Run the project's tests with `docker compose run --rm cli perl test/test_jeopardy_db.pl`.

# Backing Up and Restoring the Database

Perform a database backup with `docker compose run --rm --user "$(id -u):$(id -g)" cli ./back_up_db.sh <backup_file_name>`. This will create a backup file named `<backup_file_name>` in the `cli` directory. The backup file is a gzipped text file containing SQL comamnds.

Restore a database backup with `docker compose run --rm cli ./restore_db.sh <backup_file_name>`. The backup file named `<backup_file_name>` file must be located in the `cli` directory. **This will erase all existing data and replace it with the contents of the backup!**

# Running the Database's Command-Line Client

Running the database's command-line client will allow you to execute custom SQL queries for the purpose of inspecting or editing the data. Run the client with `docker compose run --rm cli ./open_client.sh`.
