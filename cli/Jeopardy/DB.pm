package Jeopardy::DB;
use Class::Std;
{
  use strict;
  use warnings;
  use English;
  use Error qw(:try);

  my %dbh : ATTR(init_arg => 'dbh');
  my %coryath : ATTR;
  my %standardh : ATTR;

  sub calc_outcome_distrib {
    my ($self, $args) = @_;
    my $id = ident($self);

    my @where_clauses = ();

    if (exists($args->{games})) {
      push(@where_clauses, "Answer.Game_id in (" . join(",", @{ $args->{games} }) . ")");
    }
    if (exists($args->{round})) {
      push(@where_clauses, "Answer.Round='$args->{round}'");
    }
    if (exists($args->{row})) {
      push(@where_clauses, "Answer.Row=$args->{row}");
    }
    if (exists($args->{dd})) {
      push(@where_clauses, "Answer.Daily_double=$args->{dd}");
    }

    my $where_clause = @where_clauses > 0
        ? "where " . join(" and ", @where_clauses)
        : ""
        ;

    my $sth = $dbh{$id}->prepare("select Outcome, count(*) from Answer $where_clause group by Outcome");
    if (!defined($sth)) {
      throw Error::Simple("cannot prepare statement: " . $dbh{$id}->errstr());
    }
    $sth->execute() or throw Error::Simple("cannot execute statement: " . $sth->errstr());

    my ($outcome, $count);
    $sth->bind_columns(\$outcome, \$count);

    my $counts = { t => 0, r => 0, w => 0, n => 0 };

    while ($sth->fetch()) {
      $counts->{$outcome} = $count;
    }
    $counts->{t} = $counts->{r} + $counts->{w} + $counts->{n};

    return $counts;
  }

  sub calc_num_clues {
    my ($self, $args) = @_;
    my $distrib = $self->calc_outcome_distrib($args);
    return $distrib->{t};
  }

  sub calc_num_right {
    my ($self, $args) = @_;
    my $distrib = $self->calc_outcome_distrib($args);
    return $distrib->{r};
  }

  sub calc_num_wrong {
    my ($self, $args) = @_;
    my $distrib = $self->calc_outcome_distrib($args);
    return $distrib->{w};
  }

  sub calc_num_noans {
    my ($self, $args) = @_;
    my $distrib = $self->calc_outcome_distrib($args);
    return $distrib->{n};
  }

  sub calc_prp_right {
    my ($self, $args) = @_;

    my $num_clues = $self->calc_num_clues($args);
    return ($num_clues == 0) ? 0 : $self->calc_num_right($args) / $num_clues;
  }

  sub calc_prp_wrong {
    my ($self, $args) = @_;

    my $num_clues = $self->calc_num_clues($args);
    return ($num_clues == 0) ? 0 : $self->calc_num_wrong($args) / $num_clues;
  }

  sub calc_prp_noans {
    my ($self, $args) = @_;

    my $num_clues = $self->calc_num_clues($args);
    return ($num_clues == 0) ? 0 : $self->calc_num_noans($args) / $num_clues;
  }

  sub calc_standard_score {
    my ($self, $game_id) = @_;
    my $id = ident($self);

    if (!exists($standardh{$id})) {
      $standardh{$id} = $dbh{$id}->prepare("select Round, Row, Daily_double, Wager, Outcome, Old_dollars from Game, Answer where Game.Game_id=Answer.Game_id and Game.Game_id=?");
      if ( !defined($standardh{$id}) ) {
        throw Error::Simple("cannot prepare statement: " . $dbh{$id}->errstr());
      }
    }
    $standardh{$id}->execute($game_id)
        or throw Error::Simple("cannot execute statement: " . $standardh{$id}->errstr());

    my ($round, $row, $dd, $wager, $outcome, $old_dollars);
    $standardh{$id}->bind_columns(\$round, \$row, \$dd, \$wager, \$outcome, \$old_dollars);

    my $standard_score = 0;

    while ($standardh{$id}->fetch()) {
      if (($round eq "1" or $round eq "2") and $dd == 0) {
        my $answer_value = $round * $row * ($old_dollars eq 1 ? 100 : 200);

        if ($outcome eq "r") {
          $standard_score += $answer_value;
        }
        elsif ($outcome eq "w") {
          $standard_score -= $answer_value;
        }
        else {
          # no penalty for nonresponses
        }
      }
      else {
        my $answer_value = $wager;

        if ($outcome eq "r") {
          $standard_score += $answer_value;
        }
        else {
          $standard_score -= $answer_value;
        }
      }

    }

    return $standard_score;    
  }

  sub calc_coryat_score {
    my ($self, $game_id) = @_;
    my $id = ident($self);

    # The Coryat score is the score calculated without regard to wagering. It
    # differs from the standard score in the following ways:
    #
    #   1) Correct responses on daily doubles are awarded the value of the clue
    #      instead of the value of the wager.
    #   2) No penalty is given for incorrect responses and nonresponses on daily
    #      doubles.
    #   3) Final jeopardy is not counted at all.
    
    if (!exists($coryath{$id})) {
      $coryath{$id} = $dbh{$id}->prepare("select Round, Row, Daily_double, Outcome, Old_dollars from Game, Answer where Game.Game_id=Answer.Game_id and Game.Game_id=? and Round in ('1', '2')");
      if ( !defined($coryath{$id}) ) {
        throw Error::Simple("cannot prepare statement: " . $dbh{$id}->errstr());
      }
    }
    $coryath{$id}->execute($game_id)
        or throw Error::Simple("cannot execute statement: " . $coryath{$id}->errstr());

    my ($round, $row, $dd, $outcome, $old_dollars);
    $coryath{$id}->bind_columns(\$round, \$row, \$dd, \$outcome, \$old_dollars);

    my $coryat_score = 0;

    while ($coryath{$id}->fetch()) {
      my $answer_value = $round * $row * ($old_dollars eq 1 ? 100 : 200);

      if ($outcome eq "r") {
        $coryat_score += $answer_value;
      }
      elsif ($outcome eq "w" and $dd eq 0) {
        $coryat_score -= $answer_value;
      }
    }

    return $coryat_score;
  }

  # get_games
  #
  # date_played_start - formatted YYYY-MM-DD
  # date_played_end - formatted YYYY-MM-DD
  # date_orig_start - formatted YYYY-MM-DD
  # date_orig_end - formatted YYYY-MM-DD
  # game_type_ids - array ref
  # game_ids - array ref
  #
  sub get_games {
    my ($self, $args) = @_;
    my $id = ident($self);

    my @where_clauses = ();

    if (exists($args->{date_played_start})) {
      push(@where_clauses, "Date_played >= '$args->{date_played_start}'");
    }
    if (exists($args->{date_played_end})) {
      push(@where_clauses, "Date_played <= '$args->{date_played_end}'");
    }
    if (exists($args->{date_orig_start})) {
      push(@where_clauses, "Date_orig >= '$args->{date_orig_start}'");
    }
    if (exists($args->{date_orig_end})) {
      push(@where_clauses, "Date_orig <= '$args->{date_orig_end}'");
    }
    if (exists($args->{game_type_ids})) {
      push(@where_clauses, "Note_id in (" . join(",", @{ $args->{game_type_ids} }) . ")");
    }
    if (exists($args->{game_ids})) {
      push(@where_clauses, "Game_id in (" . join(",", @{ $args->{game_ids} }) . ")");
    }

    my $where_clause = @where_clauses > 0
        ? "where " . join(" and ", @where_clauses)
        : ""
        ;

    my $sth = $dbh{$id}->prepare("select Game_id, Date_played, Date_orig, Note_id, Old_dollars from Game $where_clause");
    if ( !defined($sth) ) {
      throw Error::Simple("cannot prepare statement: " . $dbh{$id}->errstr());
    }
    $sth->execute() or throw Error::Simple("cannot execute statement: " . $sth->errstr());

    my ($game_id, $date_played, $date_orig, $game_type_id, $game_value_structure_id);
    $sth->bind_columns(\$game_id, \$date_played, \$date_orig, \$game_type_id, \$game_value_structure_id);

    my @games = ();

    while ($sth->fetch()) {
      push(@games, { id => $game_id,
                     date_played => $date_played,
                     date_orig => $date_orig,
                     type_id => $game_type_id,
                     value_structure_id => $game_value_structure_id
                   });
    }

    return \@games;
  }

  # get_game_types
  #
  # game_type_ids - array ref
  #
  sub get_game_types {
    my ($self, $args) = @_;
    my $id = ident($self);
    
    my @where_clauses = ();

    if (exists($args->{game_type_ids})) {
      push(@where_clauses, "Note_id in (" . join(",", @{ $args->{game_type_ids} }) . ")");
    }

    my $where_clause = @where_clauses > 0
        ? "where " . join(" and ", @where_clauses)
        : ""
        ;

    my $sth = $dbh{$id}->prepare("select Note_id, Note from Note $where_clause");
    if ( !defined($sth) ) {
      throw Error::Simple("cannot prepare statement: " . $dbh{$id}->errstr());
    }
    $sth->execute() or throw Error::Simple("cannot execute statement: " . $sth->errstr());

    my ($game_type_id, $game_type_name);
    $sth->bind_columns(\$game_type_id, \$game_type_name);

    my @game_types = ();

    while ($sth->fetch()) {
      push(@game_types, { id => $game_type_id,
                          name => $game_type_name
                        });
    }

    return \@game_types;
  }
}


1;
