#!/bin/bash

if [[ $# -ne 1 ]]; then
    echo "Usage: $0 <backup_file>"
    exit 1
fi

BACKUP=$1

./wait_for_db.sh

mysqldump \
    --host=$DB_HOST \
    --port=$DB_PORT \
    --user=$DB_USER \
    --password=$DB_PASSWORD \
    $DB_NAME \
    | gzip \
    > $BACKUP
