#!/bin/bash

function database_ready() {
    mysql \
        --host=$DB_HOST \
        --port=$DB_PORT \
        --user=$DB_USER \
        --password=$DB_PASSWORD \
        --execute 'status' \
        &> /dev/null
}

echo "Checking database status..."
until database_ready; do
    echo "Waiting for database to be ready..."
    sleep 1
done
echo "...ready"
