#!/bin/bash

./wait_for_db.sh

mysql \
    --host=$DB_HOST \
    --port=$DB_PORT \
    --user=$DB_USER \
    --password=$DB_PASSWORD \
    $DB_NAME
