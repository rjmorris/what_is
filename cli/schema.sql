CREATE TABLE `Answer` (
  `Answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `Game_id` int(11) NOT NULL DEFAULT '0',
  `Round` enum('1','2','f') NOT NULL DEFAULT '1',
  `Row` tinyint(4) NOT NULL DEFAULT '0',
  `Daily_double` tinyint(4) NOT NULL DEFAULT '0',
  `Outcome` enum('r','w','n') NOT NULL DEFAULT 'r',
  `Wager` int(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Answer_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `Game` (
  `Game_id` int(11) NOT NULL AUTO_INCREMENT,
  `Date_played` date NOT NULL DEFAULT '0000-00-00',
  `Note_id` int(11) NOT NULL DEFAULT '1',
  `Old_dollars` tinyint(4) NOT NULL DEFAULT '0',
  `Date_orig` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`Game_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `Note` (
  `Note_id` int(11) NOT NULL AUTO_INCREMENT,
  `Alias` varchar(5) NOT NULL DEFAULT '',
  `Note` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`Note_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

CREATE TABLE `Value` (
  `Round` enum('1','2','f') NOT NULL DEFAULT '1',
  `Row` tinyint(4) NOT NULL DEFAULT '0',
  `Old_dollars` tinyint(4) NOT NULL DEFAULT '0',
  `Value` smallint(6) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
