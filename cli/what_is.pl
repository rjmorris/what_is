use strict;
use warnings;
use English;

use DBI;
use Term::ReadLine;
use Number::Format;
use DateTime;

use Jeopardy::DB;


my $dbh = DBI->connect(
  sprintf(
    'DBI:%s:database=%s;host=%s;port=%s',
    $ENV{"DB_ENGINE"},
    $ENV{"DB_NAME"},
    $ENV{"DB_HOST"},
    $ENV{"DB_PORT"}
  ),
  $ENV{"DB_USER"},
  $ENV{"DB_PASSWORD"}
);
if ( !defined($dbh) ) {
  fatal_error("cannot connect to database: $DBI::errstr");
}
$dbh->{mysql_auto_reconnect} = 1;

my $jdb = Jeopardy::DB->new({dbh => $dbh});

my $term = new Term::ReadLine($ENV{"DB_NAME"});
$term->MinLine(undef);          # disable automatically adding lines to history
$term->ornaments('ue,,,');      # turn off underlining (see man termcap)

my $prompt = "";
my $prompt_initial = "";
my $open_game = 0;
my $game_note = 1;
my $date_aired = DateTime->now();
my $dollar_mult = 2;
my @answers = ();
my $cur_dollars = 0;
my $cur_round = "";
my $oops_clue = -1;
my $MAX_CLUES = 30;


while (1) {
  if ($open_game) {
    if ($cur_round) {
      if ($oops_clue >= 0) {
        $prompt = sprintf('** [%2d] R%s $%d => ', $oops_clue, $cur_round, $cur_dollars);
        $prompt_initial = $answers[$oops_clue - 1]->{user_entry};
      }
      else {
        $prompt = sprintf('== [%2d] R%s $%d => ', $#answers + 2, $cur_round, $cur_dollars);
        $prompt_initial = "";
      }
    }
    else {
      $prompt = "==> ";
      $prompt_initial = "";
    }
  }
  else {
    $prompt = "--> ";
    $prompt_initial = "";
  }

  my $command = input($prompt, $prompt_initial);
  $term->AddHistory($command); # manually add the command to the readline history

  if ($command =~ /^\s*new\s+(\d{4})-(\d{2})-(\d{2})\s*$/i) {
    my $really_start_new = 1;
    if ($open_game) {
      my $response = input("A game is open. Start a new game without saving <y/n>? ");
      if ($response !~ /^\s*y(es)?$/i) {
        $really_start_new = 0;
      }
    }
    if ($really_start_new) {
      $date_aired = DateTime->new(year => $1, month => $2, day => $3);
      my $word_date = $date_aired->day_abbr() . ", " . $date_aired->month_abbr() . " " . $date_aired->day() . ", " . $date_aired->year();
      confirm("starting new game that aired $word_date");
      $open_game = 1;
      $game_note = 1;
      $dollar_mult = 2;
      $cur_round = "1";
      $cur_dollars = 0;
      $oops_clue = -1;
      @answers = ();
    }
    else {
      confirm("not starting new game");
    }
  }
  elsif ($command =~ /^\s*save\s*$/i) {
    if ($open_game) {
      if ($#answers >= 0) {
        my $really_save = 1;
        if ($answers[$#answers]->{round} ne 'f') {
          my $response = input("The last clue played wasn't Final Jeopardy. Really save <y/n>? ");
          if ($response !~ /^\s*y(es)?$/i) {
            $really_save = 0;
          }
        }
        if ($really_save) {
          confirm("saving current game");

          my $gameh = $dbh->prepare("insert into Game(Date_played, Date_orig, Note_id, Old_dollars) values(curdate(), ?, ?, ?)");
          if ( !defined($gameh) ) {
            error("cannot prepare statement : " . $dbh->errstr());
            finish();
          }
          $gameh->execute(
            $date_aired->ymd(),
            $game_note,
            ( $dollar_mult == 1 ? 1 : 0 )
        )
            or error("cannot execute statement : " . $gameh->errstr()) and finish();

          my $game_id = $gameh->{'mysql_insertid'};
          confirm("assigning ID $game_id to current game");

          my $answerh = $dbh->prepare("insert into Answer(Game_id,Round,Row,Daily_double,Wager,Outcome) values(?,?,?,?,?,?)");
          if ( !defined($answerh) ) {
            error("cannot prepare statement : " . $dbh->errstr());
            finish();
          }

          foreach my $answer (@answers) {
            $answerh->execute(
              $game_id,
              $answer->{round},
              get_row($answer),
              $answer->{dd},
              $answer->{wager},
              $answer->{outcome}
          )
              or error("cannot execute statement : " . $answerh->errstr()) and finish();
          }

          $open_game = 0;
          $game_note = 1;
          $dollar_mult = 2;
          $cur_round = "";
          $cur_dollars = 0;
          $oops_clue = -1;

          confirm("game $game_id saved");
        }
        else {
          confirm("not saving game");
        }
      }
      else {
        error("no records to save");
      }
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /old/i) {
    if ($open_game) {
      confirm("using old dollar values (\$100-\$1000)");
      $dollar_mult = 1;
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /^\s*note\s+list\s*/i) {
    confirm("listing notes");

    my @notes = sort { $a->{id} <=> $b->{id} } @{ $jdb->get_game_types() };
    for my $note (@notes) {
      output("$note->{id}: $note->{name}\n");
    }
  }
  elsif ($command =~ /^\s*note\s+(\d+)\s*/i) {
    if ($open_game) {
      my $requested_note_id = $1;

      my $all_notes =  $jdb->get_game_types();
      my @requested_note = grep { $requested_note_id == $ARG->{id} } @$all_notes;

      if (@requested_note == 1) {
        confirm("setting note for current game to '$requested_note[0]->{name}'");
        $game_note = $requested_note[0]->{id};
      }
      else {
        error("invalid note id $requested_note_id");
      }
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /^\s*round\s*([12f])?\s*(\d+)?\s*$/i) {
    if ($open_game) {
      if ($oops_clue == -1) {
        my $next_round = "";
        if (defined($1)) {
          $next_round = lc($1);
        }
        else {
          if ($cur_round eq "") {
            $next_round = "1";
          }
          elsif ($cur_round eq "1") {
            $next_round = "2";
          }
          elsif ($cur_round eq "2") {
            $next_round = "f";
          }
          else {
            error("no round beyond Final Jeopardy");
          }
        }
        if (defined($2)) {
          my $start_id = $2;
          my $end_id = -1;
          my $id = $start_id - 1;

          if ($id >= 0 and $id <= $#answers) {
            my $bad_round = $answers[$id]->{round};
            for (my $id = $start_id - 1; $id <= $#answers; $id++) {
              if ($answers[$id]->{round} eq $bad_round) {
                $answers[$id]->{round} = $next_round;
                if ($id == $#answers) {
                  $cur_round = $next_round;
                }
                $end_id = $id + 1;
              }
              else {
                last;
              }
            }
            confirm("changing round to $next_round for clues $start_id-$end_id");
          }
          else {
            error("invalid clue id");
          }
        }
        else {
          confirm("starting round $next_round");
          $cur_round = $next_round;
        }
      }
      else {
        error("cannot change rounds while in oops mode");
      }
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /^\s*(\d+)\s*([rwn])\s*$/i) {
    if ($open_game) {
      if ($cur_round) {
        my $answer = parse_answer_cmd($command);

        if (defined($answer)) {
          my $round_clause = "round $cur_round";
          my $value_clause = $cur_round eq 'f' ? "\$$answer->{wager}" : "\$$answer->{value}";
          my $outcome_clause =
            $answer->{outcome} eq "r"
            ? "right"
            : (
              $answer->{outcome} eq "w"
                ? "wrong"
                : "no answer"
            )
            ;

          confirm("$round_clause, $outcome_clause $value_clause");

          if ($oops_clue >= 0) {
            $answers[$oops_clue - 1] = $answer;
            $oops_clue = -1;
            $cur_round = $answers[$#answers]->{round};
          }
          else {
            push(@answers, $answer);
          }

          $cur_dollars = calc_dollars($#answers);
        }
      }
      else {
        error("no round has been started");
      }
    }
    else {
      error("no game has been opened");
    }
  }
  elsif ($command =~ /^\s*(\d+)\s*dd\s*(\d+)\s*([rwn])\s*$/i) {
    if ($open_game) {
      if ($cur_round) {
        if ($cur_round ne 'f') {
          my $answer = parse_answer_cmd($command);

          if (defined($answer)) {
            my $round_clause = "round $cur_round";
            my $value_clause = "\$$answer->{value}";
            my $dd_clause = "(daily double, wager = \$$answer->{wager})";
            my $outcome_clause =
              $answer->{outcome} eq "r"
              ? "right"
              : ($answer->{outcome} eq "w"
                 ? "wrong"
                 : "no answer"
              );

            confirm("$round_clause, $outcome_clause $value_clause $dd_clause");

            if ($oops_clue >= 0) {
              $answers[$oops_clue - 1] = $answer;
              $oops_clue = -1;
              $cur_round = $answers[$#answers]->{round};
            }
            else {
              push(@answers, $answer);
            }

            $cur_dollars = calc_dollars($#answers);
          }
        }
        else {
          error("invalid command for round $cur_round");
        }
      }
      else {
        error("no round has been started");
      }
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /^\s*oops\s*(\d+)?\s*$/i) {
    if ($open_game) {
      if ($cur_round) {
        if ($#answers >= 0) {
          my $id = ( defined($1) ? $1 : $#answers + 1 );

          if ($id >= 1 and $id <= $#answers + 1) {
            $oops_clue = $id;
            $cur_round = $answers[$oops_clue - 1]->{round};
            $cur_dollars = calc_dollars($oops_clue - 2);
            confirm("editing answer for clue $id");
          }
          else {
            error("invalid clue id");
          }
        }
        else {
          error("no records have been created");
        }
      }
      else {
        error("no round has been started");
      }
    }
    else {
      error("no game has been opened");
    }
  }

  elsif ($command =~ /^\s*stat(\s+\d+|\s+all)?(\s+full)?\s*$/i) {
    my $game_stats = defined($1) ? $1 : "all";
    $game_stats =~ s/^\s*//;

    my $full_stats = defined($2);

    confirm("printing" . ( $full_stats ? " full " : " " ) . "statistics for" . ( ($game_stats eq "all") ? " all games" : " game $game_stats" ));

    my $games;
    if ($game_stats eq "all") {
      my @game_list = map { $ARG->{id} } @{ $jdb->get_games() };
      $games = \@game_list;
    }
    else {
      $games = [ $game_stats ];
    }

    my $counts;
    output("\n");

    $counts = $jdb->calc_outcome_distrib({games => $games});

    if ($counts->{t} > 0) {
      my $width = int(1 + log($counts->{t}) / log(10));
      output("Overall:\n");
      output("  Right: " . format_fraction($counts->{r}, $counts->{t}, $width) . "\n");
      output("  Wrong: " . format_fraction($counts->{w}, $counts->{t}, $width) . "\n");
      output("  NoAns: " . format_fraction($counts->{n}, $counts->{t}, $width) . "\n");
      output("\n");

      $counts = $jdb->calc_outcome_distrib({games => $games, round => "1"});
      output("Round 1:\n");
      output("  Right: " . format_fraction($counts->{r}, $counts->{t}, $width) . "\n");
      output("  Wrong: " . format_fraction($counts->{w}, $counts->{t}, $width) . "\n");
      output("  NoAns: " . format_fraction($counts->{n}, $counts->{t}, $width) . "\n");

      $counts = $jdb->calc_outcome_distrib({games => $games, round => "2"});
      output("Round 2:\n");
      output("  Right: " . format_fraction($counts->{r}, $counts->{t}, $width) . "\n");
      output("  Wrong: " . format_fraction($counts->{w}, $counts->{t}, $width) . "\n");
      output("  NoAns: " . format_fraction($counts->{n}, $counts->{t}, $width) . "\n");

      $counts = $jdb->calc_outcome_distrib({games => $games, round => "f"});
      output("Round f:\n");
      output("  Right: " . format_fraction($counts->{r}, $counts->{t}, $width) . "\n");
      output("  Wrong: " . format_fraction($counts->{w}, $counts->{t}, $width) . "\n");
      output("  NoAns: " . format_fraction($counts->{n}, $counts->{t}, $width) . "\n");
      output("\n");

      $counts = $jdb->calc_outcome_distrib({games => $games, dd => 1});
      output("Daily Doubles:\n");
      output("  Right: " . format_fraction($counts->{r}, $counts->{t}, $width) . "\n");
      output("  Wrong: " . format_fraction($counts->{w}, $counts->{t}, $width) . "\n");
      output("  NoAns: " . format_fraction($counts->{n}, $counts->{t}, $width) . "\n");
      output("\n");

      if ($game_stats ne "all") {
        my $fmt = Number::Format->new();
        output('Actual: $' . $fmt->format_number($jdb->calc_standard_score($game_stats)) . "\n");
        output('Coryat: $' . $fmt->format_number($jdb->calc_coryat_score($game_stats)) . "\n");
        output("\n");
      }

      if ($full_stats) {
        output("TODO: implement full stats\n");
      }
    }
    else {
      error("no results found for requested game");
    }
  }

  elsif ($command =~ /^\s*dump\s*$/) {
    confirm("dumping current records");

    for (my $i = 0; $i <= $#answers; $i++) {
      my $answer = $answers[$i];
      my $id = $i + 1;

      output("\n");
      output("id=>$id  ");
      foreach my $key (sort keys %{$answer}) {
        output("$key=>$answer->{$key}  ");
      }
    }
    output("\n");
    output("\n");
  }

  elsif ($command =~ /^\s*(help|h|\?)\s*$/i) {
    confirm("displaying help");

    output("\n");
    output("USAGE:\n");
    output("  new yyyy-mm-dd\n");
    output("  save\n");
    output("  note list|<note_id>\n");
    output("  old\n");
    output("  round [1|2|f [<clue_num>]]\n");
    output("  1|2|3|4|5|6|8|10|12|16|20 [dd [<wager>]] r|w|n\n");
    output("  oops [<clue_num>]\n");
    output("  stat [<gameid>|all] [full]\n");
    output("  dump\n");
    output("  help|h|?\n");
    output("  quit|exit|bye\n");
    output("\n");
  }

  elsif ($command =~ /^\s*(quit|exit|bye)\s*$/i) {
    my $really_quit = 1;
    if ($open_game) {
      my $response = input("A game is open. Quit without saving <y/n>? ");
      if ($response !~ /^\s*y(es)?$/i) {
        $really_quit = 0;
      }
    }
    if ($really_quit) {
      confirm("goodbye");
      finish();
    }
    else {
      confirm("not quitting");
    }
  }

  else {
    error("invalid command: $command");
  }
}



sub get_row {
  my ($ans) = @_;

  if ($ans->{round} eq '1') {
    return $ans->{value} / $dollar_mult / 100;
  }
  elsif ($ans->{round} eq '2') {
    return $ans->{value} / $dollar_mult / 200;
  }
  else {
    return 0;
  }
}

sub parse_answer_cmd {
  my ($cmd_orig) = @_;

  my $answer = { user_entry => $cmd_orig };

  my $cmd = $cmd_orig;
  $cmd =~ s/\s//;
  $cmd = lc($cmd);

  if ($cmd =~ /^(\d+)([rwn])$/) {
    $answer->{round} = $cur_round;
    $answer->{value} = ($cur_round eq 'f') ? 0 : $1 * 100;
    $answer->{dd} = 0;
    $answer->{wager} = ($cur_round eq 'f') ? $1 : 0;
    $answer->{outcome} = $2;
  }
  elsif ($cmd =~ /^(\d+)dd(\d+)([rwn])$/) {
    $answer->{round} = $cur_round;
    $answer->{value} = $1 * 100;
    $answer->{dd} = 1;
    $answer->{wager} = $2;
    $answer->{outcome} = $3;
  }
  else {
    error("invalid command $cmd_orig");
    return undef;
  }

  if ( is_valid_value($answer->{value}) ) {
    if ( is_valid_wager($answer->{wager}) ) {
      return $answer;
    }
    else {
      error("invalid wager \$$answer->{wager}");
      return undef;
    }
  }
  else {
    error("invalid value \$$answer->{value} for round $cur_round");
    return undef;
  }
}

sub is_valid_value {
  my ($value) = @_;

  return (
    $cur_round eq '1' and (   $value == $dollar_mult * 100
                                or $value == $dollar_mult * 200
                                or $value == $dollar_mult * 300
                                or $value == $dollar_mult * 400
                                or $value == $dollar_mult * 500)
      or
      $cur_round eq '2' and (   $value == $dollar_mult * 200
                                or $value == $dollar_mult * 400
                                or $value == $dollar_mult * 600
                                or $value == $dollar_mult * 800
                                or $value == $dollar_mult * 1000)
      or
      $cur_round eq 'f' and ($value == 0)
  );
}

sub is_valid_wager {
  my ($wager) = @_;

  return (
    $wager >= 0
      and
      (
        $wager <= $cur_dollars
          or
          $cur_round eq '1' and $wager <= $dollar_mult * 500
          or
          $cur_round eq '2' and $wager <= $dollar_mult * 1000
          or
          $cur_round eq 'f' and $cur_dollars < 0 and $wager == 0
      )
  );
}

sub calc_dollars {
  my ($index_end) = @_;

  my $dollars = 0;

  if ($index_end >= 0) {
    foreach my $answer (@answers[0 .. $index_end]) {
      if ($answer->{round} eq 'f' or $answer->{dd}) {
        $dollars += ( $answer->{outcome} eq 'r' ? 1 : -1 ) * $answer->{wager};
      }
      else {
        if ($answer->{outcome} eq 'r') {
          $dollars += $answer->{value};
        }
        elsif ($answer->{outcome} eq 'w') {
          $dollars -= $answer->{value};
        }
        else {
          # no change
        }
      }
    }
  }

  return $dollars;
}

sub format_fraction {
  my ($num, $den, $width) = @_;

  if (!$num) {
    $num = 0;
  }
  if (!$den) {
    $den = 0;
  }

  return sprintf("%*d/%*d (%5.1f%%)", $width, $num, $width, $den, ( $den == 0 ? 0 : $num/$den*100 ));
}

sub output {
  my ($msg) = @_;
  print STDOUT $msg;
}

sub confirm {
  my ($msg) = @_;
  output("OK: $msg\n");
}

sub error {
  my ($msg) = @_;
  output("ERROR: $msg\n");
}

sub fatal_error {
  my ($msg) = @_;
  error($msg);
  exit -1;
}

sub input {
  my ($msg, $preput) = @_;
  my $i = $term->readline($msg, $preput);
  return $i;
}

sub finish {
  $dbh->disconnect() or error("Cannot disconnect from database : " . $dbh->errstr);
  exit 0;
}
