#!/usr/bin/perl

use strict;
use warnings;
use English;

use DBI;
use Error qw(:try);
use Test::More qw(no_plan);
use Data::Dumper;

use Jeopardy::DB;


my $schema_file = "./schema.sql";


# Establish the database connection.

my $dbh = DBI->connect(
  sprintf(
    'DBI:%s:database=%s;host=%s;port=%s',
    $ENV{"DB_ENGINE"},
    $ENV{"DB_NAME"},
    $ENV{"DB_HOST"},
    $ENV{"DB_PORT"}
  ),
  $ENV{"DB_USER"},
  $ENV{"DB_PASSWORD"}
);
if ( !defined($dbh) ) {
  die "cannot connect to database: $DBI::errstr";
}

$dbh->{RaiseError} = 1;
$dbh->{PrintError} = 0;


try {
  print "=> Backing up original database tables.\n";
  backup_database();

  try {
    print "=> Creating dummy data.\n";
    create_dummy_data();

    try {
      #-------------------------------------------------------------------------------
      # Perform the tests.

      my $jdb = Jeopardy::DB->new({dbh => $dbh});


      #-----------------
      # get_game_types

      my @test_game_types;

      # using no constraints

      @test_game_types = map { $ARG->{id} } @{ $jdb->get_game_types() };
      is_deeply(\@test_game_types, [1, 2], "game types: id");

      @test_game_types = map { $ARG->{name} } @{ $jdb->get_game_types() };
      is_deeply(\@test_game_types, ["Normal", "Tournament of Champions"], "game types: name");

      # using 1 constraint

      @test_game_types = map { $ARG->{name} } @{ $jdb->get_game_types({ game_type_ids => [2] }) };
      is_deeply(\@test_game_types, ["Tournament of Champions"], "game types: name given game_type_id");

      # expecting empty set

      @test_game_types = map { $ARG->{name} } @{ $jdb->get_game_types({ game_type_ids => [179] }) };
      is_deeply(\@test_game_types, [], "game types: empty set");


      #-----------------
      # get_games

      my @test_games;

      # using no constraints

      @test_games = map { $ARG->{id} } @{ $jdb->get_games() };
      is_deeply(\@test_games, [1, 2, 3, 4], "id, all games");

      @test_games = map { $ARG->{type_id} } @{ $jdb->get_games() };
      is_deeply(\@test_games, [1, 1, 2, 2], "type_id, all games");

      @test_games = map { $ARG->{value_structure_id} } @{ $jdb->get_games() };
      is_deeply(\@test_games, [0, 1, 0, 1], "value_structure_id, all games");

      @test_games = map { $ARG->{date_played} } @{ $jdb->get_games() };
      is_deeply(\@test_games, ["1950-06-01", "1950-06-02", "1950-06-03", "1950-06-20"], "date_played, all games");

      @test_games = map { $ARG->{date_orig} } @{ $jdb->get_games() };
      is_deeply(\@test_games, ["1950-06-01", "1950-06-02", "1950-06-03", "1950-06-04"], "date_orig, all games");

      # using 1 constraint

      @test_games = map { $ARG->{date_played} } @{ $jdb->get_games({date_played_start => "1950-06-03"}) };
      is_deeply(\@test_games, ["1950-06-03", "1950-06-20"], "date_played given date_played_start");

      @test_games = map { $ARG->{id} } @{ $jdb->get_games({date_played_end => "1950-06-04"}) };
      is_deeply(\@test_games, [1, 2, 3], "id given date_played_end");

      @test_games = map { $ARG->{type_id} } @{ $jdb->get_games({date_orig_start => "1950-06-04"}) };
      is_deeply(\@test_games, [2], "type_id given date_orig_end");

      @test_games = map { $ARG->{value_structure_id} } @{ $jdb->get_games({date_orig_end => "1950-06-02"}) };
      is_deeply(\@test_games, [0, 1], "value_structure_id given date_orig_end");

      @test_games = map { $ARG->{date_orig} } @{ $jdb->get_games({game_type_ids => [2]}) };
      is_deeply(\@test_games, ["1950-06-03", "1950-06-04"], "date_orig given game_type_ids");

      @test_games = map { $ARG->{date_played} } @{ $jdb->get_games({game_ids => [1, 3]}) };
      is_deeply(\@test_games, ["1950-06-01", "1950-06-03"], "date_played given game_ids");

      # using 2 constraints

      @test_games = map { $ARG->{type_id} } @{ $jdb->get_games({date_played_start => "1950-06-02", date_played_end => "1950-06-04"}) };
      is_deeply(\@test_games, [1, 2], "type_id given date_played_start and date_played_end");

      @test_games = map { $ARG->{type_id} } @{ $jdb->get_games({date_orig_start => "1950-06-02", date_orig_end => "1950-06-04"}) };
      is_deeply(\@test_games, [1, 2, 2], "type_id given date_orig_start and date_orig_end");

      # using 3 constraints

      @test_games = map { $ARG->{id} } @{ $jdb->get_games({date_orig_start => "1950-06-02", date_orig_end => "1950-06-03", game_type_ids => [1]}) };
      is_deeply(\@test_games, [2], "id given date_orig_start, date_orig_end, and type_id");

      # expecting an empty set

      @test_games = map { $ARG->{id} } @{ $jdb->get_games({date_orig_start => "1950-07-01"}) };
      is_deeply(\@test_games, [], "id given date_orig_start and date_orig_end, empty");


      #-----------------
      # calc_num_clues

      is($jdb->calc_num_clues(), 24, "num clues, all games");
      is($jdb->calc_num_clues({games => [1]}), 5, "num clues, one game");
      is($jdb->calc_num_clues({games => [1, 3, 4]}), 16, "num clues, multiple games");
      is($jdb->calc_num_clues({round => '2'}), 8, "num clues, all games, specific round");
      is($jdb->calc_num_clues({games => [3], round => 'f'}), 0, "num clues, all games, final round");
      is($jdb->calc_num_clues({round => 'f'}), 3, "num clues, all games, final round");
      is($jdb->calc_num_clues({games => [2], round => '1', row => 4}), 2, "num clues, one game, specific round, specific row");
      is($jdb->calc_num_clues({games => [2], round => '1', row => 1}), 0, "num clues=0, one game, specific round, specific row");
      is($jdb->calc_num_clues({games => [4], row => 1}), 1, "num clues, one game, specific row");
      is($jdb->calc_num_clues({games => [3], dd => 1}), 2, "num daily doubles, one game");
      is($jdb->calc_num_clues({games => [1, 2, 3], dd => 1}), 3, "num daily doubles, multiple games");
      is($jdb->calc_num_clues({dd => 1, round => '1'}), 2, "num daily doubles, all games, specific round");
      is($jdb->calc_num_clues({games => [1, 2, 3, 4], dd => 1, round => '1', row => 5}), 1, "num daily doubles, multiple games, specific round, specific row");
      is($jdb->calc_num_clues({games => [2, 3], dd => 0}), 11, "num non daily doubles, multiple games");
      is($jdb->calc_num_clues({games => [1, 2, 3, 4], dd => 0, round => '1', row => 4}), 3, "num non daily doubles, multiple games, specific round, specific row");

      #-----------------
      # calc_num_right

      is($jdb->calc_num_right(), 12, "num right, all games");
      is($jdb->calc_num_right({games => [3]}), 2, "num right, one game");
      is($jdb->calc_num_right({games => [1, 2]}), 7, "num right, multiple games");
      is($jdb->calc_num_right({games => [4], round => '2'}), 1, "num right, one game, specific round");
      is($jdb->calc_num_right({games => [1], round => 'f'}), 0, "num right, one game, final round");
      is($jdb->calc_num_right({round => 'f'}), 1, "num right, all games, final round");
      is($jdb->calc_num_right({games => [2], round => '2', row => 1}), 1, "num right, one game, specific round, specific row");
      is($jdb->calc_num_right({games => [1], round => '1', row => 4}), 0, "num right=0, one game, specific round, specific row");
      is($jdb->calc_num_right({row => 1}), 4, "num right, all games, specific row");
      is($jdb->calc_num_right({games => [2], dd => 1}), 1, "num daily doubles right, one game");
      is($jdb->calc_num_right({games => [1, 2, 4], dd => 1}), 2, "num daily doubles right, multiple games");
      is($jdb->calc_num_right({dd => 1, round => '2'}), 1, "num daily doubles right, all games, specific round");
      is($jdb->calc_num_right({games => [2, 3, 4], dd => 1, round => '2', row => 4}), 0, "num daily doubles right, multiple games, specific round, specific row");
      is($jdb->calc_num_right({games => [1, 2, 3, 4], dd => 0}), 10, "num non daily doubles right, multiple games");
      is($jdb->calc_num_right({dd => 0, round => '2', row => 3}), 1, "num non daily doubles right, all games, specific round, specific row");


      #-----------------
      # calc_num_wrong

      is($jdb->calc_num_wrong(), 6, "num wrong, all games");
      is($jdb->calc_num_wrong({games => [1]}), 2, "num wrong, one game");
      is($jdb->calc_num_wrong({games => [3, 4]}), 2, "num wrong, multiple games");
      is($jdb->calc_num_wrong({games => [2], round => '1'}), 2, "num wrong, one game, specific round");
      is($jdb->calc_num_wrong({round => 'f'}), 1, "num wrong, all games, final round");
      is($jdb->calc_num_wrong({games => [2, 3, 4], round => 'f'}), 0, "num wrong, multiple games, final round");
      is($jdb->calc_num_wrong({games => [3], round => '2', row => 3}), 1, "num wrong, one game, specific round, specific row");
      is($jdb->calc_num_wrong({games => [3, 4], round => '2', row => 1}), 0, "num wrong=0, multiple games, specific round, specific row");
      is($jdb->calc_num_wrong({games => [4], row => 3}), 0, "num wrong, one game, specific row");
      is($jdb->calc_num_wrong({dd => 1}), 1, "num daily doubles wrong, all games");
      is($jdb->calc_num_wrong({games => [1, 2, 3, 4], dd => 1}), 1, "num daily doubles wrong, multiple games");
      is($jdb->calc_num_wrong({games => [1, 2], dd => 1, round => '1'}), 0, "num daily doubles wrong, multiple games, specific round");
      is($jdb->calc_num_wrong({dd => 1, round => '2', row => 4}), 1, "num daily doubles wrong, all games, specific round, specific row");
      is($jdb->calc_num_wrong({games => [1, 2, 3, 4], dd => 0}), 5, "num non daily doubles wrong, multiple games");
      is($jdb->calc_num_wrong({games => [2, 4], dd => 0, round => '1', row => 4}), 1, "num non daily doubles wrong, multiple games, specific round, specific row");


      #-----------------
      # calc_num_noans

      is($jdb->calc_num_noans(), 6, "num noans, all games");
      is($jdb->calc_num_noans({games => [4]}), 2, "num noans, one game");
      is($jdb->calc_num_noans({games => [1, 2, 4]}), 4, "num noans, multiple games");
      is($jdb->calc_num_noans({games => [3], round => '1'}), 2, "num noans, one game, specific round");
      is($jdb->calc_num_noans({games => [3], round => 'f'}), 0, "num noans, one game, final round");
      is($jdb->calc_num_noans({games => [2, 3, 4], round => 'f'}), 1, "num noans, multiple games, final round");
      is($jdb->calc_num_noans({round => '1', row => 4}), 1, "num noans, all games, specific round, specific row");
      is($jdb->calc_num_noans({games => [2], round => '2', row => 1}), 0, "num noans=0, one game, specific round, specific row");
      is($jdb->calc_num_noans({games => [3, 4], row => 1}), 1, "num noans, multiple games, specific row");
      is($jdb->calc_num_noans({games => [3], dd => 1}), 1, "num daily doubles noans, one game");
      is($jdb->calc_num_noans({games => [1, 2, 3, 4], dd => 1}), 1, "num daily doubles noans, multiple games");
      is($jdb->calc_num_noans({dd => 1, round => '1'}), 1, "num daily doubles noans, all games, specific round");
      is($jdb->calc_num_noans({dd => 1, round => '2', row => 3}), 0, "num daily doubles noans, all games, specific round, specific row");
      is($jdb->calc_num_noans({games => [2, 3, 4], dd => 0}), 5, "num non daily doubles noans, multiple games");
      is($jdb->calc_num_noans({games => [1, 2], dd => 0, round => '1', row => 2}), 0, "num non daily doubles noans, multiple games, specific round, specific row");


      #-----------------
      # calc_prp_right

      is($jdb->calc_prp_right({games => [2, 4]}), 7/13, "proportion right: multiple games");
      is($jdb->calc_prp_right({games => [3], row => 1}), 0.5, "proportion right: one game, specific row");
      is($jdb->calc_prp_right({round => 'f'}), 1/3, "proportion right: all games, final round");
      is($jdb->calc_prp_right({round => '1', row => 1, dd => 1}), 0, "proportion right: denominator = 0");


      #-----------------
      # calc_prp_wrong

      is($jdb->calc_prp_wrong({dd => 0}), 0.25, "proportion wrong: all games, non daily doubles");
      is($jdb->calc_prp_wrong({games => [4], round => 2}), 0, "proportion wrong: one game, specific round");
      is($jdb->calc_prp_wrong({round => '1', row => 1, dd => 1}), 0, "proportion wrong: denominator = 0");


      #-----------------
      # calc_prp_noans

      is($jdb->calc_prp_noans({games => [2]}), 0.25, "proportion noans: one game");
      is($jdb->calc_prp_noans({games => [1, 2, 3], round => '1'}), 0.3, "proportion noans: multiple games, specific round");
      is($jdb->calc_prp_noans({round => '1', row => 1, dd => 1}), 0, "proportion noans: denominator = 0");


      #-----------------
      # calc_outcome_distrib

      is_deeply($jdb->calc_outcome_distrib(), {t => 24, r => 12, w => 6, n => 6}, "outcome distrib, all games");
      is_deeply($jdb->calc_outcome_distrib({games => [3], round => '2'}), {t => 4, r => 2, w => 2, n => 0}, "outcome distrib, one game, specific round");
      is_deeply($jdb->calc_outcome_distrib({games => [1, 2, 3, 4], dd => 1}), {t => 4, r => 2, w => 1, n => 1}, "outcome distrib, multiple games, daily doubles");


      #-----------------
      # calc_standard_score

      is($jdb->calc_standard_score(2), 2700, "standard score, old value structure");
      is($jdb->calc_standard_score(3), -500, "standard score");


      #-----------------
      # calc_coryat_score

      is($jdb->calc_coryat_score(2), 700, "coryat score, old value structure");
      is($jdb->calc_coryat_score(3), 1200, "coryat score");


      #
      # End tests.
      #-------------------------------------------------------------------------------
    }
    finally {
      print "=> Removing dummy data.\n";

      try {
        remove_dummy_data($dbh);
      }
      
    }
  }
  finally {
    print "=> Restoring original database tables.\n";
    restore_database();
  }
}
finally {
  print "=> Disconnecting from database.\n";
  $dbh->disconnect();
};


exit 0;


sub backup_database {
  $dbh->do("rename table Game to backup_Game, Answer to backup_Answer, Note to backup_Note, Value to backup_Value");
}

sub restore_database {
  $dbh->do("rename table backup_Game to Game, backup_Answer to Answer, backup_Note to Note, backup_Value to Value");
}


sub create_dummy_data {
  # Read the SQL necessary to create the data tables from the schema file.

  open(my $sql, "<", $schema_file)
      or throw Error::Simple("cannot open schema file $schema_file : $OS_ERROR");

  my $create_string = "";
  while (my $line = <$sql>) {
    chomp($line);
    $create_string .= $line;
  }

  close($sql);


  # Break the SQL into single statements.

  my @create_statements = split(/;/, $create_string);


  # Create the tables in the database.

  foreach my $create_statement (@create_statements) {
    $dbh->do($create_statement);
  }


  # Insert the dummy data into the newly-created tables.

  $dbh->do("insert into Note(Note) values('Normal')");
  $dbh->do("insert into Note(Note) values('Tournament of Champions')");

  $dbh->do("insert into Game(Date_played, Date_orig) values('1950-06-01', '1950-06-01')");
  $dbh->do("insert into Game(Date_played, Date_orig, Old_dollars) values('1950-06-02', '1950-06-02', 1)");
  $dbh->do("insert into Game(Date_played, Date_orig, Note_id) values('1950-06-03', '1950-06-03', 2)");
  $dbh->do("insert into Game(Date_played, Date_orig, Note_id, Old_dollars) values('1950-06-20', '1950-06-04', 2, 1)");

  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(1, '1', 1, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(1, '1', 2, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(1, '1', 3, 'w')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(1, '1', 5, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Outcome, Wager) values(1, 'f', 'w', 400)");

  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '1', 4, 'w')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome, Daily_double, Wager) values(2, '1', 5, 'r', 1, 1000)");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '1', 4, 'n')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '1', 2, 'w')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '2', 1, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '2', 2, 'n')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(2, '2', 3, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Outcome, Wager) values(2, 'f', 'r', 1500)");

  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(3, '1', 1, 'n')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome, Daily_double, Wager) values(3, '1', 2, 'n', 1, 1100)");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(3, '2', 1, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(3, '2', 3, 'w')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome, Daily_double, Wager) values(3, '2', 4, 'w', 1, 600)");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(3, '2', 5, 'r')");

  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(4, '1', 1, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(4, '1', 3, 'n')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome) values(4, '1', 4, 'r')");
  $dbh->do("insert into Answer(Game_id, Round, Row, Outcome, Daily_double, Wager) values(4, '2', 3, 'r', 1, 400)");
  $dbh->do("insert into Answer(Game_id, Round, Outcome, Wager) values(4, 'f', 'n', 200)");
}

sub remove_dummy_data {
  $dbh->do("drop table if exists Game, Answer, Note, Value");
}
